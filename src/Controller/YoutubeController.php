<?php

namespace App\Controller;

use App\Service\MercureService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\Publisher;
use Symfony\Component\Routing\Annotation\Route;

class YoutubeController extends AbstractController
{
    /**
     * @Route("/{videoID}/pause", name="youtube_pause")
     */
    public function youtubePause(MercureService $mercure, string $videoID, Request $request) : Response
    {
        $response = $mercure->send($videoID, MercureService::PAUSE, ['time' => $request->request->get('time')]);
        return new JsonResponse(['success' => $response]);
    }

    /**
     * @Route("/{videoID}/play", name="youtube_play")
     */
    public function youtubePlay(MercureService $mercure, string $videoID, Request $request) : Response
    {
        $response = $mercure->send($videoID, MercureService::PLAY, ['time' => $request->request->get('time')]);
        return new JsonResponse(['success' => $response]);
    }

    /**
     * @Route("/{videoID}/seek", name="youtube_seek")
     */
    public function youtubeSeek(MercureService $mercure, string $videoID, Request $request) : Response
    {
        $response = $mercure->send($videoID, MercureService::SEEK, ['time' => $request->request->get('time')]);
        return new JsonResponse(['success' => $response]);
    }

     /**
     * @Route("/{videoID}/sync", name="youtube_sync")
     */
    public function youtubeSync(MercureService $mercure, string $videoID, Request $request) : Response
    {
        $response = $mercure->send($videoID, MercureService::SEEK, ['time' => $request->request->get('time')]);
        return new JsonResponse(['success' => $response]);
    }

    /**
     * @Route("/{videoID}", name="youtube_player")
     */
    public function youtubePlayer(string $videoID) : Response
    {
        return $this->render('player/youtube.html.twig', [
            'videoID' => $videoID,
        ]);
    }

}
