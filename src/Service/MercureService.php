<?php

namespace App\Service;

use Symfony\Component\Mercure\Publisher;
use Symfony\Component\Mercure\Update;

class MercureService 
{
    const PLAY = 'play';
    const PAUSE = 'pause';
    const SEEK = 'seek';

    private $publisher;
    
    public function __construct(Publisher $publisher)
    {
        $this->publisher = $publisher;
    }

    public function send(string $videoID, string $action, array $data = []): bool
    {
        $update = new Update(
            '/'.$videoID,
            json_encode(['action' => $action, 'data' => $data])
        );
        $publisher = $this->publisher;
        $publisher($update);


        return true;
    }
}